package edu.unk.fun330;

import static org.junit.jupiter.api.Assertions.*;

import java.util.stream.Collectors;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import edu.unk.fun330.Universe;
import edu.unk.fun330.base.*;
import edu.unk.fun330.controllers.ExampleShipController2;
import edu.unk.fun330.controllers.RokusekMController;

/**
 * This J-unit class tests the RokusekMController class to ensure expected behavior given certain scenarios.
 * It goes through all of the ships main priorities and then check to make sure that it is 
 * behaving as expected for certain bonus objects.
 * 
 * @author Matthew Rokusek (based given on example code)
 *
 */
class RokusekMControllerTest {

	Universe u;
	Ship ship;
	Ship ship2;
	ShipController controller;
	ControllerAction action;

	@BeforeAll
	static void beforeAll() { } // Run once before all test methods

	/**
	 * Run before each of the test methods to configure the environment.
	 * NOTE: the order of execution of the test cases is not predetermined
	 */
	@BeforeEach
	void beforeEach()
	{		
		// Create universe, add ship to it, and link controller to ship and the ship to the controller
		u = new Universe();
		ship = new Ship(100,100,0);  // ship tied to this controller under test
		u.add(ship);
		controller = new RokusekMController(ship);
		ship.setShipController(controller);
		
		// Good stuff
		LaserBonus laserBonus = new LaserBonus(800,800,1);
		EmpBonus empBonus = new EmpBonus(1100,1100,1);
		FuelBonus fuelBonus = new FuelBonus(87,87,1);
		ShieldBonus shieldBonus = new ShieldBonus(47,47,1);
		BulletBonus bulletBonus = new BulletBonus(95,95,1);
		ShipJumpBonus shipJumpBonus = new ShipJumpBonus(218,218,1);
		u.add(laserBonus);
		u.add(empBonus);
		u.add(fuelBonus);
		u.add(shieldBonus);
		u.add(bulletBonus);
		u.add(shipJumpBonus);
		

		// 'Dangerous Stuff'
		ship2 = new Ship(200,100,1);
		Bullet bullet = new Bullet(875,875,ship,1);
		BlackHole blackHole = new BlackHole(1500f,1500f,u);
		BombBonus bomb = new BombBonus(100f,10f,1);
		u.add(bomb);
		u.add(blackHole);
		u.add(bullet);
		u.add(ship2);
	}

	@AfterEach
	void afterEach() { } // Run after each of the test methods

	@AfterAll
	static void afterAll() { } // Run once after all test methods have completed

	/** 
	 * Test to verify that the ship should jump given that it has all of the stuff it needs for the kill
	 */
	@Test
	void testShipJump() {
		System.out.println("Testing to ensure that the ship is jumping if it should");
		ship.hitBy(new LaserBonus(ship.getX(),ship.getY(),1), u); // Give it a laser (it already has some, but it doesn't hurt)
		ship.hitBy(new EmpBonus(ship.getX(),ship.getY(),1), u); // Give it an EMP
		ship.hitBy(new ShipJumpBonus(ship.getX(),ship.getY(),1), u); // Give it a ship jump bonus
		assertTrue(ship.canTeleport());
		assertTrue(ship.canFireEMP());
		assertTrue(ship.canFireLaser());
		assertTrue(controller.makeMove(u) instanceof ShipJump);
	}
	
	/** 
	 * Test to check to make sure that the ship's shield is working properly
	 */
	@Test
	void testToggleShield() {
		System.out.println("Testing to ensure that the ship is toggling the shield on if it should");
		
		// Close to another ship shield check
		ship.setX(200); // make it close to another ship
		ship.setY(100);
		action = controller.makeMove(u);
		assertTrue(action instanceof ToggleShield);
		
		// Toggle-off shield check
		ship.setX(0); // move it away and make sure that it is turned off
		ship.setY(0);
		action = controller.makeMove(u);
		assertFalse(action instanceof ToggleShield);
		
		// Personal bullet shield check
		ship.setX(877); // move it close to the bullet
		ship.setY(877);
		action = controller.makeMove(u);
		assertFalse(action instanceof ToggleShield); // since the bullet is not a harmful one, nothing should happen with the shield
		
		// Bomb bonus shield check
		ship.setX(100);
		ship.setY(15);
		action = controller.makeMove(u);
		assertTrue(action instanceof ToggleShield); // shield should be turned on to protect from the bomb
		}
		

	/**
	 * Test to ensure that a new objective is getting set if there is supposed to be a new objective
	 */
	@Test
	void testNewObjective() {
		System.out.println("Testing to ensure that the ship is going to stop and look for a bonus (get a new objective)");

		action = controller.makeMove(u);
		
		// Make sure that there was a flight adjustment returned and that it told them to stop
		assertTrue(action instanceof FlightAdjustment);
		assertEquals(((FlightAdjustment)action).getStop(), true);
		}
	
	/**
	 * Test to see if a flight adjustment is properly made if a bonus is close
	 */
	@Test
	void testFlightAdjustment() 
	{
		System.out.println("Testing to ensure that the Flight adjustment is set properly for going towards a bonus");
		
		// Laser bonus
		ship.setX(900); // make it horizontal to the bonus
		ship.setY(800);
		controller.makeMove(u); // the new objective
		action = controller.makeMove(u); // the real flight adjustment
		assertTrue(action instanceof FlightAdjustment);
		FlightAdjustment fa = (FlightAdjustment)action; //expected flight adjustment
		assertEquals(fa.getFacing(),(float)Util.PI, 0.0000005);
		assertEquals(fa.getAcceleration(),1f);
		
		// Emp bonus
		ship.setX(1110); // make it horizontal to the bonus (really close, as emps are not emphasized by my ship)
		ship.setY(1100);
		controller.makeMove(u); // the new objective
		action = controller.makeMove(u); // the real flight adjustment
		assertTrue(action instanceof FlightAdjustment);
		fa = (FlightAdjustment)action; //expected flight adjustment
		assertEquals(fa.getFacing(),(float)Util.PI, 0.0000005);
		assertEquals(fa.getAcceleration(),1f);
		
		// Fuel bonus
		ship.fuel = 20;
		ship.setX(700); // the ship can be far away because the fuel is low
		ship.setY(87);
		controller.makeMove(u); // the new objective
		action = controller.makeMove(u); // the real flight adjustment
		assertTrue(action instanceof FlightAdjustment);
		fa = (FlightAdjustment)action; //expected flight adjustment
		assertEquals(fa.getFacing(),(float)Util.PI, 0.0000005);
		assertEquals(fa.getAcceleration(),1f);
	
		// Shield bonus
		ship.shield = 0;
		ship.setX(200); // the ship can be far away because the shield is out
		ship.setY(47);
		controller.makeMove(u); // the new objective
		action = controller.makeMove(u); // the real flight adjustment
		assertTrue(action instanceof FlightAdjustment);
		fa = (FlightAdjustment)action; //expected flight adjustment
		assertEquals(fa.getFacing(),(float)Util.PI, 0.0000005);
		assertEquals(fa.getAcceleration(),1f);
		
		// Bullet bonus
		ship.shield = 1000; // give the ship its health back
		ship.bullets = 0;
		ship.setX(110);
		ship.setY(95);
		controller.makeMove(u); // the new objective
		action = controller.makeMove(u); // the real flight adjustment
		assertTrue(action instanceof FlightAdjustment);
		fa = (FlightAdjustment)action; //expected flight adjustment
		assertEquals(fa.getFacing(),(float)Util.PI, 0.0000005);
		assertEquals(fa.getAcceleration(),1f);
		
		// Ship jump bonus
		ship.fuel = 2000; // fill the fuel back up
		ship.bullets = 50; // give the ship its bullets back
		ship.setX(225);
		ship.setY(218);
		controller.makeMove(u); // the new objective
		controller.makeMove(u);
		action = controller.makeMove(u); // the real flight adjustment
		assertTrue(action instanceof FlightAdjustment);
		fa = (FlightAdjustment)action; //expected flight adjustment
		assertEquals(fa.getFacing(),(float)Util.PI, 0.0000005);
		assertEquals(fa.getAcceleration(),1f);
	}
	
		
	
	/**
	 * Test to see if a the controller chooses to fire an EMP given a scenario in which it should
	 */
	@Test
	void testEmpFiring() 
	{
		System.out.println("Testing to make sure that the Emp is firing correctly");
		ship.hitBy(new EmpBonus(ship.getX(), ship.getY(),1),u);
		ship.setX(200);
		ship.setY(170);// Put the ship close to the enemy ship
		
		controller.makeMove(u); // get the new objective
		action = controller.makeMove(u);
		assertTrue(action instanceof FireEMP);
	}

	/**
	 * Test to see if a Laser is being fired if the situation is correct for the ship
	 */
	@Test
	void testLaserFiring()
	{
		System.out.println("Testing of the Laser Firing");
		ship.setSpeed(5f);
		ship.hitBy(new LaserBonus(ship.getX(), ship.getY(), 1), u);
		ship.setX(200);
		ship.setY(170);// Put the ship close to the enemy ship
		ship.setSpeed(5);
		controller.makeMove(u); // new objective
		ship.setFacing(Util.calcAngle(ship.getX(), ship.getY(), ship2.getX(), ship2.getY())); // ensures no controller action needs to be taken
		controller.makeMove(u); // makes the controller think that it had a flight adjustment
		action = controller.makeMove(u);
		assertTrue(action instanceof FireLaser);
	}
	
	/**
	 * Test to see if the behavior around a black hole is correct (shouldn't do anything)
	 */
	@Test
	void testBlackHoleBehavior()
	{
		System.out.print("Testing to ensure that no strange behavior is occuring around black holes");
		
		ship.setX(1500);
		ship.setY(1505);
		action = controller.makeMove(u);
		
		// The only thing that should happen is that the ship should toggle the
		// shield on (this won't protect it, but because the black holes don't end
		// up giving points to other players, it was not that big of a deal on the
		// performance of my ship. I made 4 different versions of my controller, and
		// two of the others avoided black holes, but this ship performs the best.
		assertTrue(action instanceof ToggleShield);
	}
	
	/**
	 * Test to ensure that a bullet is fired if is should be fired
	 */
	@Test
	void testFireBullet()
	{
		System.out.println("Testing the bullet firing");
		ship.setSpeed(5f);
		ship.laserEnergy = 0; // Make it so the should would not want to shoot a laser
		ship.setX(200);
		ship.setY(170);// Put the ship close to the enemy ship
		ship.setSpeed(5);
		controller.makeMove(u); // new objective
		ship.setFacing(Util.calcAngle(ship.getX(), ship.getY(), ship2.getX(), ship2.getY())); // ensures no controller action needs to be taken
		controller.makeMove(u); // makes the controller think that it had a flight adjustment
		action = controller.makeMove(u);
		assertTrue(action instanceof FireBullet);
	}
	/**
	 * Test to ensure that a bullet is not fired if the target is out of the range (don't waste bullets)
	 */
	@Test
	void testNotFireBullet()
	{
		// Note - everything is the same as the previous method except the ships location
		System.out.println("Testing to ensure a bullet is not fired (because of range)");
		ship.setSpeed(5f);
		ship.laserEnergy = 0; // Make it so the should would not want to shoot a laser
		ship.setX(1000);
		ship.setY(170);// Put the ship close to the enemy ship
		ship.setSpeed(5);
		controller.makeMove(u); // new objective
		ship.setFacing(Util.calcAngle(ship.getX(), ship.getY(), ship2.getX(), ship2.getY())); // ensures no controller action needs to be taken
		controller.makeMove(u); // makes the controller think that it had a flight adjustment
		action = controller.makeMove(u);
		assertFalse(action instanceof FireBullet);
	}
}
