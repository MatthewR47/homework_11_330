package edu.unk.fun330.controllers;

import java.util.Random;
import java.util.Vector;
import edu.unk.fun330.BlackHole;
import edu.unk.fun330.BombBonus;
import edu.unk.fun330.Bonus;
import edu.unk.fun330.BulletBonus;
import edu.unk.fun330.EmpBonus;
import edu.unk.fun330.FlyingObject;
import edu.unk.fun330.FuelBonus;
import edu.unk.fun330.LaserBonus;
import edu.unk.fun330.ShieldBonus;
import edu.unk.fun330.Ship;
import edu.unk.fun330.ShipJumpBonus;
import edu.unk.fun330.Universe;
import edu.unk.fun330.Util;
import edu.unk.fun330.base.Bullet;
import edu.unk.fun330.base.ControllerAction;
import edu.unk.fun330.base.FireBullet;
import edu.unk.fun330.base.FireEMP;
import edu.unk.fun330.base.FireLaser;
import edu.unk.fun330.base.FlightAdjustment;
import edu.unk.fun330.base.Laser;
import edu.unk.fun330.base.ShipController;
import edu.unk.fun330.base.ShipJump;
import edu.unk.fun330.base.ToggleShield;

/************************************************************************************************************
 * Ship controller to try to get as many points as possible. There are multiple tactics that are used.      *
 * There is a priority system in which certain types of moves (such as shipJumps) get done first. The       *
 * ship will have a dynamic search for bonuses as the ships resources are diminished (but there is a large  *
 * emphasis on lasers). It tries to conserve resources as much as possible (not bullets and lasers though)  *
 ************************************************************************************************************
 * @author Matthew Rokusek                                                                                  *
 ************************************************************************************************************/
public class RokusekMController extends ShipController
{
	private final float BULLET_DISTANCE = 500f;
	private Universe universe;
	private FlyingObject targetObjective = null;
	private ControllerAction nextAction;
	private boolean justAdjusted = false;
	private int nextMultiplyer = 1;
	
	/**
	 * Constructor to set up the Controller
	 * @param ship is the ship that the controller controls
	 */
 	public RokusekMController(Ship ship) 
	{
		super(ship);
	}

 	/**
 	 * Method to get the name of the ship
 	 */
	@Override
	public String getName() { return "Matthew"; }
	
	/**
	 * Called by the engine after each frame to get the next move of the
	 * ship/controller.
	 */
	@Override
	public ControllerAction makeMove(Universe u) 
	{
		universe = u;
		
		// Carry out the actions based upon the priority number
		switch(getPriority())
		{
		case 0: // If the ship should shield
			nextAction = new ToggleShield();
			break;
		case 1:
				nextAction = newObjective();
			break;
		case 2: // If the ship should jump
			nextAction = new ShipJump(getHighestScoreShip().getX(), getHighestScoreShip().getY()+350); // jump to the target bonus (will be a ship jump bonus)
			break;
		case 3: // If there should be a new flight adjustment
			nextAction = getFlightAdjustment();
			break;
		case 4: // If the ship should fire a laser
			nextAction = new FireLaser();
			break;
		case 5: // If the ship should fire an EMP
			nextAction = new FireEMP();
			break;
		case 6: // If the ship should fire a bullet
			nextAction = new FireBullet();
			break;
		}
				
		return nextAction;
	}

	/**
	 * Get the ship that has the highest score (other than this unless there is only one ship)
	 * @return ship with the highest score
	 */
	private FlyingObject getHighestScoreShip() 
	{
		FlyingObject highestShip = ship;// in case there is only one ship (this)
		
		// Make some other ship the highest ship
		for(FlyingObject fo : universe.getFlyingObjects())
		{
			if(fo instanceof Ship && fo != ship)
			{
				highestShip = fo;
			}
		}
		
		long highestScore = Integer.MIN_VALUE;
		for(FlyingObject ships : universe.getFlyingObjects())
		{
			if(ships instanceof Ship)
			{
				// Keep track of the highest score ship, excluding self
				if(((Ship)ships).getScore()>highestScore && ships.getX() != ship.getX() && ships.getY() != ship.getY())
				{
					highestScore = ((Ship)ships).getScore();
					highestShip = ships;
				}
			}
		}
		return highestShip;
	}

	/**
	 * Get the priority number of the move to return
	 * @return is an integer of the priority of moves to return
	 */
	private int getPriority() 
	{
		int priority = 3; // default
		
		// Depending on the situations, set the priority to the respective case
		if(shouldJump())
			priority = 2;
		else if(shouldToggleShield())
			priority = 0;
		else if(objectiveCheck())
		{
			if(shouldFireEmp())
				priority = 5;
			else if(shouldflightAdjust())
				priority = 3;
			else if(shouldFireLaser())
				priority = 4;
			else if(shouldFireBullet())
				priority = 6;
		}
		else
		{
			priority = 1;
		}
		
		return priority;
	}

	/**
	 * Check to see if the current objective is still relevant
	 * @return whether or not the current objective is still good or relevant
	 */
	private boolean objectiveCheck()
	{
		boolean stillGoodObjective = true;
		
		// Still good if the target objective is still the next good objective
		stillGoodObjective = ((targetObjective != null) && (findTargetBonus(universe.getFlyingObjects()) != null) 
				&& (targetObjective.getX() == findTargetBonus(universe.getFlyingObjects()).getX()) && 
				(targetObjective.getY() == findTargetBonus(universe.getFlyingObjects()).getY()));
		
		if(!stillGoodObjective)
			targetObjective = null; // ensures we will get a new objective
		
		return stillGoodObjective;
	}

	/**
	 * Decide whether the shield should be toggled on or off
	 * @return is a boolean of whether the shield should be toggled
	 */
	private boolean shouldToggleShield() 
	{
		boolean shouldToggleShield = false;
		for(FlyingObject fo : universe.getFlyingObjects())
		{
			if((Util.distance(ship.getX(), ship.getY(), fo.getX(), fo.getY()) <= 45))
				if(objectDangerous(fo))
					shouldToggleShield = true;
		}
		if((ship.isShieldUp() && !shouldToggleShield) || // if the shield is on and it shouldn't be
				(!ship.isShieldUp() && shouldToggleShield)) // if the shield is off and it shouldn't be
			shouldToggleShield = true;
		else
			shouldToggleShield = false;
			
		return shouldToggleShield;
	}

	/**
	 * Check to see if an object is dangerous
	 * @param fo is the flying object to be checked
	 * @return whether an object is dangerous
	 */
	private boolean objectDangerous(FlyingObject fo) 
	{
		return !fo.equals(ship) && (fo instanceof Ship || fo instanceof BombBonus || 
				(fo instanceof Bullet && ((Bullet)fo).getShipCreator() != ship)||
				fo instanceof BlackHole || fo instanceof Laser);
	}

	/**
	 * Check to see if the ship should teleport
	 * @return whether the ship should teleport
	 */
	private boolean shouldJump()
	{		
		// only teleport if there is the equipment necessary for a kill
		boolean shouldJump = false;
		if (ship.canFireEMP() && ship.canTeleport() && ship.canFireLaser())
		{
			shouldJump = true;
		}
		return shouldJump;
	}

	/**
	 * Check to see if a flight adjustment should be made
	 * @return whether a flight adjustment should be made
	 */
	private boolean shouldflightAdjust() 
	{
		boolean shouldAdjust = false;
		
		
		// if the laser is going to be used then the angle should be more direct
		if(shouldFireLaser() && Math.abs(ship.getFacing() - Util.calcAngle(ship.getX(), 
				ship.getY(), getClosestShip().getX(), getClosestShip().getY())) > .1)
		{
			shouldAdjust = true;
		}
		else if(!justAdjusted) // in the case that the bullet will be fired then only make an adjustment every other time
		{
			shouldAdjust = true;
		}
		else
		{
			justAdjusted = false;
		}
		return shouldAdjust;
	}

	/**
	 * Check to see if the laser should be fired
	 * @return whether the laser should be fired
	 */
	private boolean shouldFireLaser()
	{			// if it is in the range
		return ship.canFireLaser() && Util.distance(getClosestShip().getNextX(2), 
				getClosestShip().getNextY(2), ship.getX(), ship.getY()) <= BULLET_DISTANCE-35;
	}

	/**
	 * Check to see if the ship should fire an EMP
	 * @return whether the ship should fire an EMP
	 */
	private boolean shouldFireEmp()
	{    // Fire it if in range
		return (ship.canFireEMP() && (Util.distance(getClosestShip().getX(), 
				getClosestShip().getY(), ship.getX(), ship.getY()) < 333));
	}
	
	/**
	 * Check to see if a bullet should be fired
	 * @return whether a bullet should be fired
	 */
	private boolean shouldFireBullet()
	{
		return Util.distance(getClosestShip().getNextX(nextMultiplyer), 
				getClosestShip().getNextY(nextMultiplyer), ship.getX(), 
				ship.getY()) <= BULLET_DISTANCE-100; // change it a bit to make sure that the target is hit
	}

	/**
	 * Get a new flight adjustment
	 * @return a new flight adjustment
	 */
	private FlightAdjustment getFlightAdjustment()
	{
		FlightAdjustment nextAdjust = new FlightAdjustment();
		
		// If the ship is moving, aim at nearby ships
		if(ship.getSpeed() >= 5)
		{
			if(ship.canFireLaser())
				nextAdjust.setFacing(getLaserFireAngle(getClosestShip()));
			else
				nextAdjust.setFacing(getBulletFireAngle(getClosestShip()));
			justAdjusted = true;
		}
		else // otherwise start moving towards the objective
		{
			nextAdjust.setFacing(faceCoordinatesAngle(targetObjective));
			nextAdjust.setAcceleration(1f);
		}
		
		return nextAdjust;
	}
	
	/**
	 * Get a new objective 
	 * @return is a controller action that is the result of setting a new objective
	 */
	private ControllerAction newObjective()
	{
		ControllerAction nextAction= new FlightAdjustment();
		
		targetObjective = findTargetBonus(universe.getFlyingObjects()); // Get the object that makes the most sense to get
		
		if(targetObjective != null) // only stop the ship if there is an objective object to go towards
			((FlightAdjustment) nextAction).setStop(true);
		
		return nextAction;
	}

	/**
	 * Get the angle to a flying object from the ship
	 * @param target is the target flying object
	 * @return an angle in radians to the flying object target from the ship
	 */
	private float faceCoordinatesAngle(FlyingObject target) 
	{
		float angle = Util.calcAngle(ship.getX(), ship.getY(), target.getX(), target.getY());
		return angle;
	}

	/**
	 * Get the most logical target bonus given a collection of objects
	 * @param objectList is a Vector of flying objects that are eligible for the bonus search
	 * @return is the best option for a bonus to target
	 */
	private FlyingObject findTargetBonus(Vector<FlyingObject> objectList) 
	{
		FlyingObject closestObject = null; // set the closest ship as your own so that it does not throw a null error if there are no other ships
		float objectDistance = Float.MAX_VALUE;
		
		// Loop through all of the flying objects and get the one that is the closes to the ship
		for (FlyingObject flyingObject : objectList)
		{
			if(!(flyingObject instanceof BombBonus) &&       //skip bombs
					flyingObject instanceof Bonus) //only look for bonuses
			{
				float distance = Util.distance (ship.getX(),ship.getY(),flyingObject.getX(),flyingObject.getY());
			
				// If a resource is low, then the distance to the object will be comparitively low
				if(flyingObject instanceof ShieldBonus)
					distance = distance * (ship.getShieldHealth()/1000f); 
				else if(flyingObject instanceof FuelBonus)
				{
					distance = distance * (ship.getFuelAmmount()/2000f)*2;
				}
				else if(flyingObject instanceof BulletBonus)
				{
					distance = distance * (ship.getBulletsAmmount()/50);
				}
				else if(flyingObject instanceof LaserBonus)
				{
					distance = distance * .1f; // there should always be an emphasis on lasers because they are so good
				}
				else if(flyingObject instanceof EmpBonus)
				{
					if(!ship.canFireEMP())
						distance *= .7f;
					else
						distance *=2;
				}
				else if(flyingObject instanceof ShipJumpBonus)
				{
					if(!ship.canTeleport())
						distance *= .8f;
				}
				
				if(distance < objectDistance)
				{
					objectDistance = distance;
					closestObject = flyingObject;
				}	
			}			
		}
		return closestObject; 
	}

	/**
	 * Get the angle to be used when firing a bullet
	 * @param targetShip is the ship that will be shot at
	 * @return is the angle to be used when firing a bullet
	 */
	private float getBulletFireAngle(FlyingObject targetShip)
	{
		// Have a bit of a random spread
		Random r = new Random();
		nextMultiplyer += 3;
		nextMultiplyer %= 25;
		float angle = Util.calcAngle(ship.getX(), ship.getY(), 
				targetShip.getNextX(nextMultiplyer), targetShip.getNextY(nextMultiplyer));
		return angle + ((r.nextFloat((int)((.08+.08)*10+1))-.08f*10) / 10.0f);
	}
	
	/**
	 * Get the angle to be used when firing a laser
	 * @param targetShip is the ship to get shot at
	 * @return is the angle to be used when firing a laser
	 */
	private float getLaserFireAngle(FlyingObject targetShip)
	{
		float angle = Util.calcAngle(ship.getX(), ship.getY(), targetShip.getNextX(8), targetShip.getNextY(8));
		return angle;
	}

	/**
	 * Get the closest ship
	 * @return the ship that is the closest
	 */
	private FlyingObject getClosestShip()
	{
		FlyingObject closestShip = ship; // set the closest ship as your own so that it does not throw a null error if there are no other ships
		float objectDistance = Float.MAX_VALUE;
		
		// Loop through all of the flying objects and get the one that is the closes to the ship
		for (FlyingObject flyingObject : universe.getFlyingObjects())
		{
			if((ship != flyingObject) &&             //skip my own ship
					flyingObject instanceof Ship) //only look for other ships
			{
				float distance = Util.distance (ship.getX(),ship.getY(),flyingObject.getX(),flyingObject.getY());
			
				if(distance < objectDistance)
				{
					objectDistance = distance;
					closestShip = flyingObject;
				}
			}	
		}
		return closestShip; 
	}
}