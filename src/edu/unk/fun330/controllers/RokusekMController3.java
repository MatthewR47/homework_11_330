package edu.unk.fun330.controllers;

import java.util.Random;
import java.util.Vector;

import edu.unk.fun330.BlackHole;
import edu.unk.fun330.BombBonus;
import edu.unk.fun330.Bonus;
import edu.unk.fun330.BulletBonus;
import edu.unk.fun330.EmpBonus;
import edu.unk.fun330.FlyingObject;
import edu.unk.fun330.FuelBonus;
import edu.unk.fun330.LaserBonus;
import edu.unk.fun330.ShieldBonus;
import edu.unk.fun330.Ship;
import edu.unk.fun330.ShipJumpBonus;
import edu.unk.fun330.Universe;
import edu.unk.fun330.Util;
import edu.unk.fun330.base.Bullet;
import edu.unk.fun330.base.ControllerAction;
import edu.unk.fun330.base.FireBullet;
import edu.unk.fun330.base.FireEMP;
import edu.unk.fun330.base.FireLaser;
import edu.unk.fun330.base.FlightAdjustment;
import edu.unk.fun330.base.Laser;
import edu.unk.fun330.base.ShipController;
import edu.unk.fun330.base.ShipJump;
import edu.unk.fun330.base.ToggleShield;

/**
 * @author Matthew
 * 
 * Ship controller to try to get as many points as possible. There are multiple tactics that are used.
 * There is a priority system in which certain types of moves (such as shipJumps) get to do first.
 * The ship tries to make an attempt at avoiding the top two players if they have more than two lasers. 
 * The ship also will try to target the top two players if they are low on health
 * 
 */
public class RokusekMController3 extends ShipController
{
	private final float BULLET_DISTANCE = 500f;
	private Universe universe;
	private FlyingObject targetObjective = null;
	private ControllerAction nextAction;
	private int shieldHealth = ship.getShieldHealth();
	private boolean justAdjusted = false;
	private int nextMultiplyer =1;
	
	/**
	 * Constructor to set up the Controller
	 * @param ship is the ship that the controller controls
	 */
 	public RokusekMController3(Ship ship) 
	{
		super(ship);
	}

	@Override
	public String getName() { return "Matthew-3"; }
	
	/**
	 * Called by the engine after each frame to get the next move of the
	 * ship/controller.
	 * 
	 */
	@Override
	public ControllerAction makeMove(Universe u) 
	{
		universe = u;
		
		// Carry out the actions based upon the priority number
		switch(getPriority())
		{
		case 0: // If the ship should shield
			nextAction = new ToggleShield();
			break;
		case 1:
			if(checkForDanger() == null)
				nextAction = newObjective();
			else
				nextAction = getNewSafeObjective(checkForDanger());
			break;
		case 2: // If the ship should jump
			nextAction = new ShipJump(getHighestScoreShip().getX(), getHighestScoreShip().getY()+350); // jump to the target bonus (will be a ship jump bonus)
			break;
		case 3: // If there should be a new flight adjustment
			nextAction = getFlightAdjustment();
			break;
		case 4: // If the ship should fire a laser
			nextAction = new FireLaser();
			break;
		case 5: // If the ship should fire an EMP
			nextAction = new FireEMP();
			break;
		case 6: // If the ship should fire a bullet
			nextAction = new FireBullet();
			break;
		default: // if there is no danger, then reset the ship using a normal objective
			if(checkForDanger() == null)
				nextAction = newObjective();
			else // otherwise use the danger mindful method
				nextAction = getNewSafeObjective(checkForDanger());
		}
				
		return nextAction;
	}

	/**
	 * Get the ship that has the highest score (other than this unless there is only one ship)
	 * @return ship with the highest score
	 */
	private FlyingObject getHighestScoreShip() 
	{
		FlyingObject highestShip = ship;
		long highestScore = 0;
		for(FlyingObject ships : universe.getFlyingObjects())
		{
			if(ships instanceof Ship)
			{
				// Keep track of the highest score ship, excluding self
				if(((Ship)ships).getScore()>highestScore && ships.getX() != ship.getX() && ships.getY() != ship.getY())
				{
					highestScore = ((Ship)ships).getScore();
					highestShip = ships;
				}
			}
		}
		return highestShip;
	}
	
	/**
	 * Get the ship with the second highest score
	 * @return the ship with the second highest score
	 */
	private FlyingObject getSecondHighestScoreShip()
	{
		FlyingObject highestShip = ship;
		long secondHighestScore = 0;
		for(FlyingObject ships : universe.getFlyingObjects())
		{
			if(ships instanceof Ship)
			{
				// Keep track of the ship, excluding self
				if(((Ship)ships).getScore()>secondHighestScore && !(ships.getX() == ship.getX() && ships.getY() == ship.getY()) &&
						!(ships.getX() == getHighestScoreShip().getX() && ships.getY() == getHighestScoreShip().getY()))
				{
					secondHighestScore = ((Ship)ships).getScore();
					highestShip = ships;
				}
			}
		}
		return highestShip;
	}

	/**
	 * Get the priority number of the move to return
	 * @return is an integer of the priority of moves to return
	 */
	private int getPriority() 
	{
		int priority = 3; // default
		
		// Depending on the situations, set the priority to the respective case
		if(shouldJump())
			priority = 2;
		else if(shouldToggleShield())
			priority = 0;
		else if(objectiveCheck())
		{
			if(shouldFireEmp())
				priority = 5;
			else if(shouldflightAdjust())
				priority = 3;
			else if(shouldFireLaser())
				priority = 4;
			else if(shouldFireBullet())
				priority = 6;
		}
		else
		{
			priority = 1;
		}
		
		return priority;
	}

	/**
	 * Check to see if the current objective is still relevant
	 * @return whether or not the current objective is still good or relevant
	 */
	private boolean objectiveCheck()
	{
		boolean stillGoodObjective = true;
		
		// Still good if the target objective is still the next good objective
		stillGoodObjective = ((targetObjective != null) && (findTargetBonus(universe.getFlyingObjects()) != null) && (targetObjective.getX() == findTargetBonus(universe.getFlyingObjects()).getX()) && (targetObjective.getY() == findTargetBonus(universe.getFlyingObjects()).getY()));
		
		// If the objective should be on a safe target
		if((targetObjective != null) &&(checkForDanger() != null))
			if(targetObjective == getSafeBonus(checkForDanger()))
				stillGoodObjective = true;
			else
				stillGoodObjective = false;
			
		
		if(!stillGoodObjective)
			targetObjective = null; // ensures we will get a new objective
		
		return stillGoodObjective;
	}
	
	/**
	 * Get the flying objects to be used in the search for the most logical choice of a target bonus
	 * @param <FlyingObjects> the flying objects
	 * @return the Vector of flying objects that will be used for finding the target bonus
	 */
	private Vector<FlyingObject> getLogicalFlyingObjects()
	{
		Vector<FlyingObject> logicalObjects = new Vector<FlyingObject>();
		
		if(((((Ship)getHighestScoreShip()).getShieldHealth() < 250) && ship.getShieldHealth() > 500)) 
			for(FlyingObject fo : universe.getFlyingObjects())
			{
				if(Util.distance(fo.getX(), fo.getY(), getHighestScoreShip().getX(), getHighestScoreShip().getY())<450)
					logicalObjects.add(fo);
			}
		if(((((Ship)getSecondHighestScoreShip()).getShieldHealth() < 250) && ship.getShieldHealth() > 500)) 
			for(FlyingObject fo : universe.getFlyingObjects())
			{
				if(Util.distance(fo.getX(), fo.getY(), getSecondHighestScoreShip().getX(), getSecondHighestScoreShip().getY())<450)
					logicalObjects.add(fo);
			}
		if(logicalObjects.size() == 0)
		{
			logicalObjects = universe.getFlyingObjects();
		}
			
		
		return logicalObjects;
	}
	
	/**
	 * See if there is a dangerous object in the vicinity of the ship
	 * @return the dangerous object that is in the vicinity of the ship
	 */
	private FlyingObject checkForDanger() 
	{
		FlyingObject dangerousObject = null;
		
		FlyingObject bestShip = getHighestScoreShip();
		FlyingObject nextBest = getSecondHighestScoreShip();
		
		// Loop through all of the objects and keep track i fther is a ship that has more than two lasers and is close or if there is a black hole
		for(FlyingObject fo: universe.getFlyingObjects())
		{
			if(((fo.getX() == nextBest.getX()) && (fo.getY() == nextBest.getY())) && ((((Ship)nextBest).getLaserAmount() >2)))
			{
				if(Util.distance(fo.getX(), fo.getY(), ship.getX(), ship.getY()) < 650)
				{
				dangerousObject = fo;
				}
			}
			if(((fo.getX() == bestShip.getX()) && (fo.getY() == bestShip.getY())) && ((((Ship)bestShip).getLaserAmount() >2)))
			{
				if(Util.distance(fo.getX(), fo.getY(), ship.getX(), ship.getY()) < 650)
				{
				dangerousObject = fo;
				}
			}
			if(fo instanceof BlackHole)
			{
				if(Util.distance(fo.getX(), fo.getY(), ship.getX(), ship.getY()) < 600)
				{
				dangerousObject = fo;
				}
			}
		}
		
		return dangerousObject;
	}

	/**
	 * Get 'safe' bonus target
	 * @param thingToAvoid is the dangerous object that needs to be avoided
	 * @return is the safest bonus object to go towards
	 */
	private FlyingObject getSafeBonus(FlyingObject thingToAvoid)
	{
		if(thingToAvoid == null)
			return findTargetBonus(getLogicalFlyingObjects());
		
		Vector<FlyingObject> safeObjects = new Vector<FlyingObject>();
		FlyingObject safeBonus = null;
		
		double angleToDanger = faceCoordinatesAngle(thingToAvoid);
		
		// If the object is in the other direction, then it can be added to the list of items that are safe
		for(FlyingObject fo : universe.getFlyingObjects())
		{
			if((Math.abs(angleToDanger-faceCoordinatesAngle(fo)) > (3.14*5)/6) && (Math.abs(angleToDanger-faceCoordinatesAngle(fo)) > (3.14*7)/6))
			{
				safeObjects.add(fo);
			}
		}
		// If the object is in the 
		if(safeObjects.size() == 0)
		{
			for(FlyingObject fo : universe.getFlyingObjects())
			{
				if((Math.abs(angleToDanger-faceCoordinatesAngle(fo)) > 3.14/2) && (Math.abs(angleToDanger-faceCoordinatesAngle(fo)) > (3.14*3)/2))
				{
					safeObjects.add(fo);
				}
			}
		}
		// If there are not any options for places to go that are safe, just use any of them
		if(safeObjects.size() == 0)
		{
			safeBonus = findTargetBonus(getLogicalFlyingObjects());
		}
		else
		{
			safeBonus = findTargetBonus(safeObjects);
		}
		
		return safeBonus;
	}
	
	/**
	 * Get the controller action that is safe
	 * @param thingToAvoid is the dangerous object that is causing the issue
	 * @return is the safe controller action
	 */
	private ControllerAction getNewSafeObjective(FlyingObject thingToAvoid)
	{
		ControllerAction nextAction= new FlightAdjustment();
		
		targetObjective = getSafeBonus(thingToAvoid);
		
		if(targetObjective != null) // only stop the ship if there is an objective object to go towards
			((FlightAdjustment) nextAction).setStop(true);
		
		return nextAction;
	}

	/**
	 * Decide whether the shield should be toggled on or off
	 * @return is a boolean of whether the shield should be toggled
	 */
	private boolean shouldToggleShield() 
	{
		boolean shieldOn = checkShield();
		boolean shouldToggleShield = false;
		for(FlyingObject fo : universe.getFlyingObjects())
		{
			if((Util.distance(ship.getX(), ship.getY(), fo.getX(), fo.getY()) <= 45))
				if(objectDangerous(fo))
					shouldToggleShield = true;
		}
		if((shieldOn && !shouldToggleShield) || // if the shield is on and it shouldn't be
				(!shieldOn && shouldToggleShield)) // if the shield is off and it shouldn't be
			shouldToggleShield = true;
		else
			shouldToggleShield = false;
			
		return shouldToggleShield;
	}
	
	/**
	 * See if the shield is being used
	 * @return whether the shield is being used
	 */
	private boolean checkShield() 
	{
		boolean shieldOn = false;
		if(shieldHealth > ship.getShieldHealth()) // see if any shield has been used to see if it is on
			shieldOn = true;
		shieldHealth = ship.getShieldHealth(); // update the shield health
		return shieldOn;
	}

	/**
	 * Check to see if an object is dangerous
	 * @param fo is the flying object to be checked
	 * @return whether an object is dangerous
	 */
	private boolean objectDangerous(FlyingObject fo) 
	{
		return !fo.equals(ship) && (fo instanceof Ship || fo instanceof BombBonus || fo instanceof Bullet || fo instanceof BlackHole || fo instanceof Laser);
	}

	/**
	 * Check to see if the ship should teleport
	 * @return whether the ship should teleport
	 */
	private boolean shouldJump()
	{		
		// only teleport if there is the equipment necessary for a kill
		boolean shouldJump = false;
		if (ship.canFireEMP() && ship.canTeleport() && ship.canFireLaser())
		{
			shouldJump = true;
		}
		return shouldJump;
	}

	/**
	 * Check to see if a flight adjustment should be made
	 * @return whether a flight adjustment should be made
	 */
	private boolean shouldflightAdjust() 
	{
		boolean shouldAdjust = false;
		
		// if the laser is going to be used then the angle should be more direct
		if(shouldFireLaser() && Math.abs(ship.getFacing() - Util.calcAngle(ship.getX(), ship.getY(), getClosestShip().getX(), getClosestShip().getY())) > .1)
		{
			shouldAdjust = true;
		}
		else if(!justAdjusted) // in the case that the bullet will be fired then only make an adjustment every other time
		{
			shouldAdjust = true;
		}
		else
		{
			justAdjusted = false;
		}
		return shouldAdjust;
	}

	/**
	 * Check to see if the laser should be fired
	 * @return whether the laser should be fired
	 */
	private boolean shouldFireLaser()
	{			// if it is in the range
		return ship.canFireLaser() && Util.distance(getClosestShip().getNextX(2), getClosestShip().getNextY(2), ship.getX(), ship.getY()) <= BULLET_DISTANCE-35;
	}

	/**
	 * Check to see if the ship should fire an EMP
	 * @return whether the ship should fire an EMP
	 */
	private boolean shouldFireEmp()
	{    // Fire it if in range
		return (ship.canFireEMP() && (Util.distance(getClosestShip().getX(), getClosestShip().getY(), ship.getX(), ship.getY()) < 333));
	}
	
	/**
	 * Check to see if a bullet should be fired
	 * @return whether a bullet should be fired
	 */
	private boolean shouldFireBullet()
	{
		Bullet testBullet = new Bullet(ship.getX(), ship.getY(),ship, 1); // simulate a bullet
		return Util.distance(getClosestShip().getNextX(nextMultiplyer), getClosestShip().getNextY(nextMultiplyer), testBullet.getNextX(nextMultiplyer), testBullet.getNextY(nextMultiplyer)) <= BULLET_DISTANCE-100; // change it a bit to make sure that the target is hit
	}

	/**
	 * Get a new flight adjustment
	 * @return a new flight adjustment
	 */
	private FlightAdjustment getFlightAdjustment()
	{
		FlightAdjustment nextAdjust = new FlightAdjustment();
		
		// If the ship is moving, aim at nearby ships
		if(ship.getSpeed() >= 5)
		{
			if(ship.canFireLaser())
				nextAdjust.setFacing(getLaserFireAngle(getClosestShip()));
			else
				nextAdjust.setFacing(getBulletFireAngle(getClosestShip()));
			justAdjusted = true;
		}
		else // otherwise start moving towards the objective
		{
			nextAdjust.setFacing(faceCoordinatesAngle(targetObjective));
			nextAdjust.setAcceleration(1f);
		}
		
		return nextAdjust;
	}
	
	/**
	 * Get a new objective 
	 * @return is a controller action that is the result of setting a new objective
	 */
	private ControllerAction newObjective()
	{
		ControllerAction nextAction= new FlightAdjustment();
		
		targetObjective = findTargetBonus(getLogicalFlyingObjects()); // Get the object that makes the most sense to get
		
		if(targetObjective != null) // only stop the ship if there is an objective object to go towards
			((FlightAdjustment) nextAction).setStop(true);
		
		return nextAction;
	}

	/**
	 * Get the angle to a flying object from the ship
	 * @param target is the target flying object
	 * @return an angle in radians to the flying object target from the ship
	 */
	private float faceCoordinatesAngle(FlyingObject target) 
	{
		float angle = Util.calcAngle(ship.getX(), ship.getY(), target.getX(), target.getY());
		return angle;
	}

	/**
	 * Get the most logical target bonus given a collection of objects
	 * @param objectList is a Vector of flying objects that are eligible for the bonus search
	 * @return is the best option for a bonus to target
	 */
	private FlyingObject findTargetBonus(Vector<FlyingObject> objectList) 
	{
		FlyingObject closestObject = null; // set the closest ship as your own so that it does not throw a null error if there are no other ships
		float objectDistance = Float.MAX_VALUE;
		
		// Loop through all of the flying objects and get the one that is the closes to the ship
		for (FlyingObject flyingObject : objectList)
		{
			if(!(flyingObject instanceof BombBonus) &&       //skip bombs
					flyingObject instanceof Bonus) //only look for bonuses
			{
				float distance = Util.distance (ship.getX(),ship.getY(),flyingObject.getX(),flyingObject.getY());
			
				// If a resource is low, then the distance to the object will be comparitively low
				if(flyingObject instanceof ShieldBonus)
					distance = distance * (ship.getShieldHealth()/1000)-1f; // the -1 is in case the shield and gas are out
				else if(flyingObject instanceof FuelBonus)
				{
					distance = distance * (ship.getFuelAmmount()/2000);
				}
				else if(flyingObject instanceof BulletBonus)
				{
					distance = distance * (ship.getBulletsAmmount()/50) + 25f; // the +1 is in case the fuel and gas are out 
				}
				else if(flyingObject instanceof LaserBonus)
				{
					distance = distance * .2f; // there should always be an emphasis on lasers because they are so good
				}
				else if(flyingObject instanceof EmpBonus)
				{
					if(!ship.canFireEMP())
						distance *= .7f;
					else
						distance *=2;
				}
				else if(flyingObject instanceof ShipJumpBonus)
				{
					if(!ship.canTeleport())
						distance *= .8f;
				}
				
				if(distance < objectDistance)
				{
					objectDistance = distance;
					closestObject = flyingObject;
				}	
			}			
		}
		return closestObject; 
	}

	/**
	 * Get the angle to be used when firing a bullet
	 * @param targetShip is the ship that will be shot at
	 * @return is the angle to be used when firing a bullet
	 */
	private float getBulletFireAngle(FlyingObject targetShip)
	{
		// Have a bit of a random spread
		Random r = new Random();
		nextMultiplyer += 3;
		nextMultiplyer %= 47;
		float angle = Util.calcAngle(ship.getX(), ship.getY(), targetShip.getNextX(nextMultiplyer), targetShip.getNextY(nextMultiplyer));
		return angle + ((r.nextFloat((int)((.1+.1)*10+1))-.1f*10) / 10.0f);
	}
	
	/**
	 * Get the angle to be used when firing a laser
	 * @param targetShip is the ship to get shot at
	 * @return is the angle to be used when firing a laser
	 */
	private float getLaserFireAngle(FlyingObject targetShip)
	{
		float angle = Util.calcAngle(ship.getX(), ship.getY(), targetShip.getNextX(8), targetShip.getNextY(8));
		return angle;
	}

	/**
	 * Get the closest ship
	 * @return the ship that is the closest
	 */
	private FlyingObject getClosestShip()
	{
		FlyingObject closestShip = ship; // set the closest ship as your own so that it does not throw a null error if there are no other ships
		float objectDistance = Float.MAX_VALUE;
		
		// Loop through all of the flying objects and get the one that is the closes to the ship
		for (FlyingObject flyingObject : universe.getFlyingObjects())
		{
			if(!ship.equals(flyingObject) &&             //skip my own ship
					flyingObject instanceof Ship) //only look for other ships
			{
				float distance = Util.distance (ship.getX(),ship.getY(),flyingObject.getX(),flyingObject.getY());
			
				if(distance < objectDistance)
				{
					objectDistance = distance;
					closestShip = flyingObject;
				}
			}	
		}
		return closestShip; 
	}
}
