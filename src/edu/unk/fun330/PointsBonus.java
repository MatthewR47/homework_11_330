package edu.unk.fun330;

import java.awt.*;

/**
 * Gives ship a score boost.
 * @author John Hastings
 *
 */
public class PointsBonus extends Bonus {
	
	public static Image pointsImage;
	
	public PointsBonus(float x, float y, int amount) {
		super(x, y, amount);
	}

	public Color getColor() { return Color.CYAN; }
	public Image getImage() {return pointsImage;};

}
