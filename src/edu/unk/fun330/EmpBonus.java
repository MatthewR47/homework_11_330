package edu.unk.fun330;

import java.awt.Image;

/**
 * Provides a ship with EMP blast capabilities.
 * @author John Hastings
 *
 */
public class EmpBonus extends Bonus {

	public static Image empBonusImage;

	//int points;

	public EmpBonus(float x, float y, int amount) { super(x, y, amount); }

	//public Color getColor() { return Constants.bulletsColor; }
	public Image getImage() {return empBonusImage;};
}
