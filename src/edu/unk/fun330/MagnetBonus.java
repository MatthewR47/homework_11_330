package edu.unk.fun330;

import java.awt.Image;

/**
 * Gives ship a timed magnet which sweeps up nearby good bonuses
 * @author John Hastings
 */
public class MagnetBonus extends Bonus {
	public static Image magnetBonusImage;
	
	//int points;
	
	public MagnetBonus(float x, float y, int amount) {
		super(x, y, amount);
	}

	//public Color getColor() { return Constants.bulletsColor; }
	public Image getImage() {return magnetBonusImage;};
	
}
