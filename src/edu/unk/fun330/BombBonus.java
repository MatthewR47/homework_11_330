package edu.unk.fun330;

import java.awt.*;
import edu.unk.fun330.base.EMP;

public class BombBonus extends Bonus {

	public static Image bombImage;
	
	public BombBonus(float x, float y, int amount) {
		super(x, y, amount);
	}

	public Color getColor() { return Constants.bombColor; }
	public Image getImage() {return bombImage;};
	
	// return true if the object should be removed after being hit
	protected boolean hitBy(FlyingObject fo, Universe u) {
		if (fo instanceof EMP) return true;
		else return super.hitBy(fo, u);
	}
}
