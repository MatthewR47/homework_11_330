package edu.unk.fun330;

import java.awt.Image;

/**
 * Give ship an expiring bonus to jump/teleport to a new location.
 * @author John Hastings
 *
 */
public class ShipJumpBonus extends Bonus {

public static Image shipJumpBonusImage;
	
	public ShipJumpBonus(float x, float y, int amount) { super(x, y, amount); }
	public Image getImage() {return shipJumpBonusImage;};
}
