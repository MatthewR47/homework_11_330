package edu.unk.fun330;

import java.awt.*;

/**
 * Resupplies ships with bullets.
 * @author John Hastings
 */
public class BulletBonus extends Bonus {

	public static Image bulletBonusImage;
	
	int points;
	
	public BulletBonus(float x, float y, int amount) {
		super(x, y, amount);
	}

	public Color getColor() { return Constants.bulletsColor; }
	public Image getImage() {return bulletBonusImage;};

}
